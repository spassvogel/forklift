﻿using System;
using System.Collections;
using System.Collections.Generic;
using Eductation21CC.Forklift.Controllers;
using Eductation21CC.Forklift.UI;
using Eductation21CC.Forklift.World;
using UnityEngine;

namespace Eductation21CC.Forklift
{
    /**
     * Scenarios arrange all the elements in the scene to play out a
     * specific scenario
     * */
    public class Scenarios : MonoBehaviour
    {
        [SerializeField] private ForkliftUI forkliftUI;
        [SerializeField] private World.Forklift forklift;
        [SerializeField] private Transform mast;
        [SerializeField] private Transform carriage;
        [SerializeField] private Transform pallet;
        [SerializeField] private PalletContactTrigger palletContactTrigger;
        [SerializeField] private Transform cargoBig;
        [SerializeField] private Transform cargoMedium;
        [SerializeField] private Transform cargoSmall;
        [SerializeField] private Transform cargoTall;
        [SerializeField] private CompositeCargo cargoComposite;


        void Start()
        {
            Sandbox();
        }

        public void StartScenario(int index)
        {
            switch(index)
            {
                case 0:
                    Sandbox();
                    break;
                case 1:
                    StartCoroutine(BigCargoTilted());
                    break;
                case 2:
                    StartCoroutine(BigCargoNotTilted());
                    break;
                case 3:
                    StartCoroutine(TallCargoVertical());
                    break;
                case 4:
                    StartCoroutine(TallCargoHorizontal());
                    break;
            }
        }


        private void ResetScenario()
        {
            // Hide all cargo
            cargoBig.gameObject.SetActive(false);
            cargoMedium.gameObject.SetActive(false);
            cargoSmall.gameObject.SetActive(false);
            cargoTall.gameObject.SetActive(false);
            cargoComposite.gameObject.SetActive(false);


            // Reset rotations of cargo
            cargoComposite.Reset();
            cargoMedium.transform.rotation = Quaternion.Euler(Vector3.zero);
            cargoSmall.transform.rotation = Quaternion.Euler(Vector3.zero);
            cargoBig.transform.rotation = Quaternion.Euler(Vector3.zero);
            cargoTall.transform.rotation = Quaternion.Euler(Vector3.zero);

            // Reset forklift
            forklift.transform.rotation = Quaternion.Euler(Vector3.zero);
            forklift.transform.localPosition = new Vector2(-14, 0);

            // Stop movement of pallet
            Rigidbody2D rigidbody = pallet.GetComponent<Rigidbody2D>();
            rigidbody.velocity = Vector2.zero;
            rigidbody.angularVelocity = 0;
            // Pallet gets parented back to the scenarios gameobject
            pallet.parent = transform;

            InputController.Instance.ReleaseButtons();
        }


        private void Sandbox()
        {
            ResetScenario();

            // Set UI
            forkliftUI.sandboxMode = true;
            forkliftUI.SetText("Sandbox mode! Have fun");

            forklift.transform.rotation = Quaternion.Euler(Vector3.zero);
            forklift.transform.localPosition = new Vector2(-8.88f, 0);

            pallet.transform.localPosition = new Vector2(-0.46f, -3.75f);
            pallet.transform.rotation = Quaternion.Euler(0, 0, 0);

            switch(UnityEngine.Random.Range(0, 2))
            {
                case 0:
                    // Variation 1
                    cargoMedium.gameObject.SetActive(true);
                    cargoSmall.gameObject.SetActive(true);
                    cargoMedium.transform.localPosition = new Vector2(-1.056053f, -1.432999f);
                    cargoSmall.transform.localPosition = new Vector2(0.4602583f, -1.666916f);
                    break;

                case 1:
                    cargoComposite.gameObject.SetActive(true);
                    cargoComposite.transform.localPosition = new Vector2(-0.33f, -1.26f);
                    break;
            }

        }

        IEnumerator BigCargoTilted ()
        {
            // Scenario 1
            // Forklift drives with big cargo on pallet. Carriage is tilted
            ResetScenario();


            forkliftUI.sandboxMode = false;
            forkliftUI.SetText(@"When moving load, tilt the carriage backwards.
This shifts the center of gravity back");

            var tiltAngle = 7;
            cargoBig.gameObject.SetActive(true);


            carriage.transform.localPosition = new Vector2(carriage.transform.localPosition.x, -0.18f);
            mast.transform.rotation = Quaternion.Euler(0, 0, tiltAngle);

            pallet.transform.localPosition = new Vector2(-8.2f, -2.5f);
            pallet.transform.rotation = Quaternion.Euler(0, 0, tiltAngle);

            cargoBig.transform.localPosition = new Vector2(-8.5f, -0.13f);
            cargoBig.transform.rotation = Quaternion.Euler(0, 0, tiltAngle);

            yield return new WaitForSeconds(1);

            InputController.Instance.PressRightButton();
            while (forklift.transform.localPosition.x < -3)
            {
                yield return null;
            }
            InputController.Instance.ReleaseButtons();

            yield return null;
        }

        IEnumerator BigCargoNotTilted ()
        {
            // Scenario 2
            // Forklift drives with big cargo on pallet. Carriage is not tilted so
            // cargo will fall
            ResetScenario();

            forkliftUI.sandboxMode = false;
            forkliftUI.SetText(@"When moving load, tilt the carriage backwards.
This shifts the center of gravity back");

            cargoComposite.gameObject.SetActive(true);

            forklift.transform.rotation = Quaternion.Euler(Vector3.zero);

            var tiltAngle = 0;
            carriage.transform.localPosition = new Vector2(carriage.transform.localPosition.x, -0.18f);
            mast.transform.rotation = Quaternion.Euler(0, 0, tiltAngle);

            pallet.transform.localPosition = new Vector2(-8.13f, -2.95f);
            pallet.transform.rotation = Quaternion.Euler(0, 0, tiltAngle);

            cargoComposite.transform.localPosition = new Vector2(-7.8f, -0.51f);
            cargoComposite.transform.rotation = Quaternion.Euler(0, 0, tiltAngle);

            yield return new WaitForSeconds(1);

            InputController.Instance.PressRightButton();
            while (forklift.transform.localPosition.x < -3)
            {
                yield return null;
            }
            InputController.Instance.ReleaseButtons();

            yield return null;
        }

        IEnumerator TallCargoVertical ()
        {
            // Scenario 3
            // Forklift drives with tall cargo vertical
            ResetScenario();

            forkliftUI.sandboxMode = false;
            forkliftUI.SetText(@"Place tall cargo in a vertical manner,
flush to the carriage");

            cargoTall.gameObject.SetActive(true);

            var tiltAngle = 0;
            forklift.transform.localPosition = new Vector2(-6.150001f, 0);

            carriage.transform.localPosition = new Vector2(carriage.transform.localPosition.x, -0.99f);
            mast.transform.rotation = Quaternion.Euler(0, 0, tiltAngle);

            pallet.transform.localPosition = new Vector2(-0.53f, -3.74982f);
            pallet.transform.rotation = Quaternion.Euler(0, 0, tiltAngle);
            palletContactTrigger.Connect(); // Pallet normally gets connected to the carriage
                                            // when it comes into contact. However because we manually
                                            // move, it doesn't. So here we force the connection

            cargoTall.transform.localPosition = new Vector2(-1.30f, -0.45f);
            cargoTall.transform.rotation = Quaternion.Euler(0, 0, 90);

            yield return new WaitForSeconds(1);

            InputController.Instance.PressCarriageUpButton();
            yield return new WaitForSeconds(3);
            InputController.Instance.ReleaseButtons();

            yield return null;
        }

        IEnumerator TallCargoHorizontal ()
        {
            // Scenario 3
            // Forklift drives with tall cargo vertical
            ResetScenario();

            forkliftUI.sandboxMode = false;
            forkliftUI.SetText(@"Place tall cargo in a vertical manner,
flush to the carriage");

            cargoTall.gameObject.SetActive(true);


            var tiltAngle = 0;
            forklift.transform.localPosition = new Vector2(-6.150001f, 0);
            carriage.transform.localPosition = new Vector2(carriage.transform.localPosition.x, -0.99f);
            mast.transform.rotation = Quaternion.Euler(0, 0, tiltAngle);

            pallet.transform.localPosition = new Vector2(-0.53f, -3.74982f);
            pallet.transform.rotation = Quaternion.Euler(0, 0, tiltAngle);
            palletContactTrigger.Connect(); // Pallet normally gets connected to the carriage
                                            // when it comes into contact. However because we manually
                                            // move, it doesn't. So here we force the connection

            cargoTall.transform.localPosition = new Vector2(0.9000462f, -2.661161f);
            cargoTall.transform.rotation = Quaternion.Euler(0, 0, tiltAngle);

            yield return new WaitForSeconds(1);

            InputController.Instance.PressCarriageUpButton();
            yield return new WaitForSeconds(3);
            InputController.Instance.ReleaseButtons();

            yield return null;
        }

    

        IEnumerator DriveForwardAndLift ()
        {
            forklift.transform.localPosition = new Vector2(-11.32374f, forklift.transform.localPosition.y);

            int i = 0;
            // Move forward
            while (forklift.transform.localPosition.x < -4.232853f)
                //while (i++ < 1000)
            {
                InputController.Instance.PressRightButton();
                yield return null;
            }
            InputController.Instance.ReleaseButtons();
            yield return null;
        }
    }
}