﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Eductation21CC.Forklift.UI
{
    public class ForkliftUI : MonoBehaviour
    {
        [SerializeField] private GameObject sandboxControls; 
        [SerializeField] private TMPro.TextMeshProUGUI infoText; 

        public void SetText(string text)
        {
            infoText.text = text;
        }

        public bool sandboxMode
        {
            set { sandboxControls.SetActive(value);  }
        }
    }
}