﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Eductation21CC.Forklift.Controllers;
using UnityEngine;
using UnityEngine.UI;

namespace Eductation21CC.Forklift.UI
{
    public class SNESController : MonoBehaviour
    {
        [SerializeField] private Image LeftButton;
        [SerializeField] private Image RightButton;
        [SerializeField] private Image CarriageUpButton;
        [SerializeField] private Image CarriageDownButton;
        [SerializeField] private Image TiltBackButton;
        [SerializeField] private Image TiltForwardButton;

        [SerializeField] private Color normalColor;
        [SerializeField] private Color pressedColor;

        [DllImport("__Internal")]
        private static extern void CallToggleHelp();

        void Update()
        {
            LeftButton.color = InputController.Instance.LeftButtonPressed ? pressedColor : normalColor;
            RightButton.color = InputController.Instance.RightButtonPressed ? pressedColor : normalColor;
            CarriageUpButton.color = InputController.Instance.CarriageUpButtonPressed ? pressedColor : normalColor;
            CarriageDownButton.color = InputController.Instance.CarriageDownButtonPressed ? pressedColor : normalColor;
            TiltBackButton.color = InputController.Instance.TiltBackButtonPressed ? pressedColor : normalColor;
            TiltForwardButton.color = InputController.Instance.TiltForwardButtonPressed ? pressedColor : normalColor;
        }

        public void ToggleHelp()
        {
            CallToggleHelp();
        }
    }
}
