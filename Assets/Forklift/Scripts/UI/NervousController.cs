﻿using System.Collections;
using System.Collections.Generic;
using Eductation21CC.Forklift.Controllers;
using UnityEngine;

namespace Eductation21CC.Forklift.UI
{
    public class NervousController : MonoBehaviour
    {
        [SerializeField] private Animator animator;

        public void Start()
        {
            animator.SetBool("nervous", true);
        }

        public void Update()
        {
            // This controller is nervous untilc licked

            if (InputController.Instance.AnyButtonPressed) {
                animator.SetBool("nervous", false);
            }

        }

        public void OnClick()
        {
            animator.SetBool("nervous", false);
        }

    }
}