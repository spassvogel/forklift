﻿using System.Collections;
using System.Collections.Generic;
using Eductation21CC.Forklift.World;
using UnityEngine;

namespace Eductation21CC.Forklift
{
    public class Game : MonoBehaviour
    {
        [SerializeField] private World.Forklift forklift;
        [SerializeField] private Transform pallet;
        [SerializeField] private CompositeCargo cargoComposite;
        [SerializeField] private Transform rack;
        [SerializeField] private TMPro.TextMeshProUGUI timeText;
        [SerializeField] private float time;
        [SerializeField] private Floor floor;

        private Vector2 forkliftStart;
        private Vector2 palletStart;
        private Vector2 rackStart;
        private Vector2 cargoStart;

        // Start is called before the first frame update
        void Start()
        {
            forkliftStart = forklift.transform.localPosition;
            palletStart = pallet.localPosition;
            rackStart = rack.localPosition;
            cargoStart = cargoComposite.transform.localPosition;

            Reset();
        }

        // Update is called once per frame
        void Update()
        {
            time += Time.deltaTime;
            timeText.text = FormatTime();
        }

        public string FormatTime()
        {
            int minutes = (int)time / 60;
            int seconds = (int)time - 60 * minutes;

            return $"{minutes:00}:{seconds:00}";
        }

        public void Reset()
        {
            forklift.transform.localPosition = forkliftStart;
            forklift.transform.eulerAngles = Vector3.zero;
            forklift.Reset();

            cargoComposite.Reset();
            cargoComposite.transform.localPosition = cargoStart;
            cargoComposite.transform.eulerAngles = Vector3.zero;


            rack.localPosition = rackStart;

            pallet.parent = transform;
            pallet.transform.localPosition = palletStart;
            pallet.transform.eulerAngles = Vector3.zero;

            floor.TemporaryDisable();
        }
    }
}