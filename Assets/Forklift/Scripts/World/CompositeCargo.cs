﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Eductation21CC.Forklift.World
{

    public class CompositeCargo : MonoBehaviour
    {
        [SerializeField] private Transform[] boxes;

        private Vector2[] originalPositions;
        private void Awake()
        {
            originalPositions = new Vector2[boxes.Length];
            // Store positions to be able to reset it later
            for (int i = 0; i < boxes.Length; i++)
            {
                originalPositions[i] = boxes[i].localPosition;
            }
        }

        public void Reset()
        {
            for (int i = 0; i < boxes.Length; i++)
            {
                boxes[i].localPosition = originalPositions[i];
                boxes[i].rotation = Quaternion.Euler(Vector3.zero);

                Rigidbody2D rigidbody = boxes[i].GetComponent<Rigidbody2D>();
                rigidbody.velocity = Vector2.zero;
                rigidbody.angularVelocity = 0;
            }
        }
    }
}
