﻿using System.Collections;
using System.Collections.Generic;
using Eductation21CC.Forklift.Controllers;
using UnityEngine;

namespace Eductation21CC.Forklift.World
{

    public class Forklift : MonoBehaviour
    {
        [SerializeField] private Transform mast;
        [SerializeField] private Transform carriage;
        [SerializeField] private Transform rearWheel;
        [SerializeField] private Transform frontWheel;
        [SerializeField] private new Rigidbody2D rigidbody2D;

        [SerializeField] private Rigidbody2D pallet; // Todo: set this dynamiccally when forklift touches pallet

        [SerializeField] private float maxVelocity = 2f; // Lateral speed max
        [SerializeField] private float acceleration = 2f; // How fast we speed up 
        [SerializeField] private float decceleration = 4f; // How fast we slow down
        [SerializeField] private float carriageSpeed = 0.01f;  // Up down speed
        [SerializeField] private float tiltSpeed = 0.03f;
        [SerializeField] private AudioSource palletDropAudio;

        private float velocity = 0;

        private float carriageYMin = -0.99f;
        private float carriageYMax = 4;

        private float mastTiltMin = 0;
        private float mastTiltMax = 7;

  
        void Update()
        {
            rigidbody2D.constraints = RigidbodyConstraints2D.None;
            pallet.constraints = RigidbodyConstraints2D.None;
            pallet.bodyType = RigidbodyType2D.Dynamic;

            // Lateral movement
            //float lateralMove = 0;
            if (InputController.Instance.LeftButtonPressed)
            {
                //lateralMove = -1;
                if (velocity >= -maxVelocity)
                {
                    // Accelerating back
                    velocity -= acceleration * Time.deltaTime;
                }
            }
            else if (InputController.Instance.RightButtonPressed)
            {
                //lateralMove = 1;
                if (velocity <= maxVelocity)
                {
                    // Accelerating forward
                    velocity += acceleration * Time.deltaTime;
                }                
            }
            else
            {
                // Decelerating
                if (velocity > decceleration * Time.deltaTime)
                {
                    velocity -= decceleration * Time.deltaTime;
                }
                else if (velocity < -decceleration * Time.deltaTime)
                {
                    velocity += decceleration * Time.deltaTime;
                }
                else
                {
                    velocity = 0;
                }
            }      
            rigidbody2D.velocity = new Vector2(velocity, rigidbody2D.velocity.y);

            // Mast tilt
            float tilt = 0;
            if (InputController.Instance.TiltBackButtonPressed)
            {
                tilt = 1;
            }
            else if (InputController.Instance.TiltForwardButtonPressed)
            {
                tilt = -1;
            }

            if (tilt != 0)
            {                
                var zRotation = mast.transform.eulerAngles.z;
                zRotation = Mathf.Clamp(zRotation + tilt * tiltSpeed, mastTiltMin, mastTiltMax);
                mast.transform.eulerAngles = new Vector3(0.0f, 0.0f, zRotation);
                //print(zRotation);
            }

            // Carriage move
            float carriageMove = 0;
            if (InputController.Instance.CarriageUpButtonPressed)
            {
                carriageMove = 1;
            }
            else if (InputController.Instance.CarriageDownButtonPressed)
            {
                carriageMove = -1;
            }
            if (carriageMove != 0)
            {
                var y = Mathf.Clamp(carriage.localPosition.y + carriageMove * carriageSpeed, carriageYMin, carriageYMax);
                carriage.localPosition = new Vector3(carriage.localPosition.x, y, 0);

                if (y == carriageYMin)
                {
                    palletDropAudio.Play();
                }

                pallet.bodyType = RigidbodyType2D.Kinematic;
            }

            if (carriageMove != 0 || tilt != 0)
            {
                // The gravitational force of any cargo pushing down together with the carriage moving up
                // seems to push the pallet laterally. I don't see any other way than to temporarily constrain
                // the lateral movement of the forklift and the pallet
                rigidbody2D.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
                //pallet.constraints = RigidbodyConstraints2D.FreezePositionX;
                pallet.bodyType = RigidbodyType2D.Kinematic;
            }

            // Rotate wheels
            frontWheel.localRotation = Quaternion.AngleAxis(transform.position.x * -25, Vector3.forward);
            rearWheel.localRotation = Quaternion.AngleAxis(transform.position.x * -25, Vector3.forward);
        }

        public void Reset()
        {
            mast.transform.eulerAngles = new Vector3(0.0f, 0.0f, 0f);
            carriage.transform.localPosition = new Vector2(2.18f, -0.99f);
        }
    }
}