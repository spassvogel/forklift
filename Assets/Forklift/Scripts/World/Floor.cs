﻿using System;
using System.Collections;
using System.Collections.Generic;
using Eductation21CC.Forklift.Controllers;
using UnityEngine;

namespace Eductation21CC.Forklift.World
{
    // Plays a crashing sound when a box fallls
    public class Floor : MonoBehaviour
    {
        [SerializeField] private AudioSource cargoCrashAudio;


        private void OnCollisionEnter2D(Collision2D other)
        {
            //if (other.gameObject.name.StartsWith("Cargo"))
            //{
            if (enabled)
            {
                cargoCrashAudio.Play();
            }
            //}
        }

        internal void TemporaryDisable()
        {
            enabled = false;
            StartCoroutine(EnableAfter1Second());
        }

        private IEnumerator EnableAfter1Second()
        {
            yield return new WaitForSeconds(2);
            enabled = true;
        }
    }

}