﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Eductation21CC.Forklift.World
{
    // Shadow is always projected straight down. Even if something weird with the forklift happens 
    public class ForkliftShadow : MonoBehaviour
    {
        [SerializeField] private Transform forklift;

        private float offset;

        // Needs to start at the right position relative to the forklift
        void Start()
        {
            offset = forklift.localPosition.x - transform.localPosition.x;
        }

        void Update()
        {
            transform.localPosition = new Vector2(forklift.localPosition.x - offset, transform.localPosition.y);
        }
    }
}