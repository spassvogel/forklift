﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Eductation21CC.Forklift.World
{

    public class PalletContactTrigger : MonoBehaviour
    {
        [SerializeField] private Transform pallet;
        [SerializeField] private Collider2D carriage;


        private Transform originalPalletParent; 

        private void Awake()
        {
            // Normal pallet parent is stored (this will be Scenario)
            originalPalletParent = pallet.parent;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if(carriage == other)
            {
                Connect();
            }
        }

        //When the Primitive exits the collision, it will change Color
        private void OnTriggerExit2D(Collider2D other)
        {
            if (carriage == other)
            {
                pallet.parent = originalPalletParent;
            }
        }

        public void Connect()
        {
            pallet.parent = carriage.transform;
        }
    }

}
