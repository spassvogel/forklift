﻿
using UnityEngine;
using System.Collections;
using System;

namespace Eductation21CC.Forklift.Controllers

{
    public class SoundController : MonoBehaviour
    {
        [SerializeField] private AudioSource audioPlayer;
        [SerializeField] private AudioClip driveSound;
        [SerializeField] private AudioClip reverseSound;
        [SerializeField] private AudioClip tiltSound;
        [SerializeField] private AudioClip carriageSound;


        private float startVolume;
        private IEnumerator fader;

        private void Awake()
        {
            startVolume = audioPlayer.volume;
        }

        // Update is called once per frame
        void Update()
        {
            if (InputController.Instance.CarriageDownButtonPressed || InputController.Instance.CarriageUpButtonPressed)
            {
                if (audioPlayer.clip != carriageSound)
                {
                    PlaySound(carriageSound);
                }
            }
            else if (InputController.Instance.LeftButtonPressed)
            {
                if (audioPlayer.clip != reverseSound)
                {
                    PlaySound(reverseSound);
                }

            }
            else if (InputController.Instance.RightButtonPressed)
            {
                if (audioPlayer.clip != driveSound)
                {
                    PlaySound(driveSound);
                }
            }
            else if (InputController.Instance.TiltBackButtonPressed || InputController.Instance.TiltForwardButtonPressed)
            {
                if (audioPlayer.clip != tiltSound)
                {
                    PlaySound(tiltSound);
                }
            }
            else
            {
                if (audioPlayer.clip != null)
                {

                    fader = FadeOut(3);
                    StartCoroutine(fader);
                }
            }
        }

        private void PlaySound(AudioClip carriageSound)
        {
            if (fader != null)
            {
                StopCoroutine(fader);
            }

            audioPlayer.clip = carriageSound;
            audioPlayer.volume = startVolume;
            audioPlayer.Play();
        }

        private IEnumerator FadeOut(float fadeTime)
        {

            while (audioPlayer.volume > 0)
            {
                audioPlayer.volume -= startVolume * Time.deltaTime / fadeTime;
                yield return null;
            }

            audioPlayer.Stop();
            audioPlayer.clip = null;
        }
    }
}