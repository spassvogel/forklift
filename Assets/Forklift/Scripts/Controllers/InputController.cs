﻿using System;
using System.Collections;
using System.Collections.Generic;
using Eductation21CC.Forklift.Common;
using UnityEngine;

namespace Eductation21CC.Forklift.Controllers
{
    public class InputController : Singleton<InputController>
    {
        private bool leftButtonPressed;
        private bool rightButtonPressed;
        private bool carriageUpButtonPressed;
        private bool carriageDownButtonPressed;
        private bool tiltBackButtonPressed;
        private bool tiltForwardButtonPressed;

        public void PressLeftButton()
        {
            leftButtonPressed = true;
        }

        public void PressRightButton()
        {
            rightButtonPressed = true;
        }

        public void PressCarriageUpButton()
        {
            carriageUpButtonPressed = true;
        }

        public void PressCarriageDownButton()
        {
            carriageDownButtonPressed = true;
        }

        public void PressTiltBackButton()
        {
            tiltBackButtonPressed = true;
        }

        public void PressTiltForwardButton()
        {
            tiltForwardButtonPressed = true;
        }

        public bool LeftButtonPressed
        {
            get
            {
                return leftButtonPressed || Input.GetAxis("Horizontal") == -1;
            }
        }

        public bool RightButtonPressed
        {
            get
            {
                return rightButtonPressed || Input.GetAxis("Horizontal") == 1;
            }
        }

        public bool CarriageUpButtonPressed
        {
            get
            {
                return carriageUpButtonPressed || Input.GetAxis("Vertical") == 1;
            }
        }

        public bool CarriageDownButtonPressed
        {
            get
            {
                return carriageDownButtonPressed || Input.GetAxis("Vertical") == -1;
            }
        }

        public bool TiltBackButtonPressed
        {
            get
            {
                return tiltBackButtonPressed || Input.GetKey(KeyCode.LeftBracket);
            }
        }

        public bool TiltForwardButtonPressed
        {
            get
            {
                return tiltForwardButtonPressed || Input.GetKey(KeyCode.RightBracket);
            }
        }

        public bool AnyButtonPressed
        {
            get
            {
                return leftButtonPressed ||
                    rightButtonPressed ||
                    carriageUpButtonPressed ||
                    carriageDownButtonPressed ||
                    tiltBackButtonPressed ||
                    tiltForwardButtonPressed;
            }
        }

        public void ReleaseButtons()
        {
            leftButtonPressed = false;
            rightButtonPressed = false;
            carriageUpButtonPressed = false;
            carriageDownButtonPressed = false;
            tiltBackButtonPressed = false;
            tiltForwardButtonPressed = false;
        }

        public void Update()
        {

            if (Input.GetMouseButtonUp(0))
            {
                ReleaseButtons();
            }
           
        }  
    }

}
